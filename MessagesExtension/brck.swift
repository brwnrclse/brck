//
//  brck.swift
//  brck
//
//  Created by Brown Recluse on 4/23/17.
//  Copyright © 2017 Brown Recluse. All rights reserved.
//

import Foundation
import Messages

/// A `brck` has a custom limited set of colors for text and backgrounds
extension UIColor {
    enum ColorName: UInt32 {
        case Apple = 0xffff3243
        case Bubble = 0xffff3fff
        case Concrete = 0xff606060
        case Ice = 0xff26daff
        case Salmon = 0xffec6b4e
    }
}

extension UIColor {
    convenience init(named name: ColorName) {
        let rgbaVal = name.rawValue
        let red = CGFloat((rgbaVal >> 24) & 0xff) / 255.0
        let green = CGFloat((rgbaVal >> 16) & 0xff) / 255.0
        let blue = CGFloat((rgbaVal >> 8) & 0xff) / 255.0
        let alpha = CGFloat((rgbaVal) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}

/// A Brck has content and color.
struct brck {
    // MARK: Properties
    var content: String?
}
