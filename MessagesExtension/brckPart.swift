//
//  brckPart.swift
//  brck
//
//  Created by Brown Recluse on 4/23/17.
//  Copyright © 2017 Brown Recluse. All rights reserved.
//

import UIKit

/**
 * An individual piece of brck, aka a "bat"
 */
protocol Bat {
    var rawValue: String { get }
    var img: UIImage { get }
    var stickerImg: UIImage { get }
}
